# jelalis

Site consultable sur http://jelalis.ledeuxiemetexte.fr

Thème dérivé du thème Lattes de MoozThemes (https://moozthemes.com/lattes-free-one-page-bootstrap-theme/) qui contient notamment les fichiers à installer dans les dossiers `js`, `fonts` et `css` : https://github.com/moozthemes/lattes/

Dossier `dist` de LeafletExtraMarkers (https://github.com/coryasilva/Leaflet.ExtraMarkers) à installer à la racine du site.