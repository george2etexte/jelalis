<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
		<meta name="description" content="">
		<meta name="author" content="">
		<link rel="icon" href="favicon.ico">
		<title>Défi #JeLaLis : les autrices choisies</title>
		<!-- Bootstrap core CSS -->
		<link href="css/bootstrap.min.css" rel="stylesheet">
		<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.1/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
		<link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/themes/smoothness/jquery-ui.css">
		<!-- Custom styles for this template -->
		<link href="css/owl.carousel.css" rel="stylesheet">
		<link href="css/owl.theme.default.min.css"  rel="stylesheet">
		<link href="css/style.css" rel="stylesheet">
		<!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
		<!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
		<script src="js/ie-emulation-modes-warning.js"></script>
		<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		<![endif]-->
		
		
 <link rel="stylesheet" href="https://unpkg.com/leaflet@1.5.1/dist/leaflet.css"
   integrity="sha512-xwE/Az9zrjBIphAcBb3F6JVqxf46+CDLwfLMHloNu6KEQCAWi6HcDUbeOfBIptF7tcCzusKFjFw2yuvEpDL9wQ=="
   crossorigin=""/>
    <!-- Make sure you put this AFTER Leaflet's CSS -->
 <script src="https://unpkg.com/leaflet@1.5.1/dist/leaflet.js"
   integrity="sha512-GffPMF3RvMeYyc1LWMHtK8EbPv0iNZ8/oTtHPx9/cc2ILxQ+u905qIwdpULaqDkyBKgOaB57QTMg7ztg8Jm2Og=="
   crossorigin=""></script>

 <link rel="stylesheet" href="./dist/css/leaflet.extra-markers.min.css">
 <script src="./dist/js/leaflet.extra-markers.min.js"></script>

	</head>
<?php

include("connect.php");

?>
	<body id="page-top">
	<!--
	<div style="background-color:rgba(0,0,0,0.5);position:fixed;left:0px;bottom:0px;right:0px;height:110px;z-index:1000;text-align:center;padding:20px;color:white;text-shadow: 2px 2px 5px black;font-size:1.3em;">
	<b>SITE DE D&Eacute;MONSTRATION, CONTENU NON D&Eacute;FINITIF.</b><br/>
	<span style="color:red;"><b>DÉBUT DE L'OPÉRATION LE  2 AVRIL 2019.<br/>
	MERCI DE NE PAS LA DIFFUSER AVANT CE JOUR.</b></span><br/>
	</div>
	-->
		<!-- Navigation -->
		<nav class="navbar navbar-default navbar-fixed-top">
			<div class="container">
				<!-- Brand and toggle get grouped for better mobile display -->
				<div class="navbar-header page-scroll" style="padding-top:15px">
					<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					</button>
					<a class="navbar-brand page-scroll" href="./index.php">
					<span style="color:#c71E17;font-family:'Open Sans',sans-serif;">
					<b>JE LA LIS !</b>
					</span>
					</a>
				</div>
				<!-- Collect the nav links, forms, and other content for toggling -->
				<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
					<ul class="nav navbar-nav navbar-right">
						<li class="hidden">
							<a href="#page-top"></a>
						</li>
						<li>
							<a class="page-scroll" href="#autrices">Les autrices choisies</a>
						</li>
						<li>
							<a class="page-scroll" href="#actions">Les actions réalisées</a>
						</li>
						<li>
							<a class="page-scroll" href="#faq">Foire aux questions</a>
						</li>
					</ul>
				</div>
				<!-- /.navbar-collapse -->
			</div>
			<!-- /.container-fluid -->
		</nav>
		<!-- Header -->
		<header>
			<div class="container">
				<div class="slider-container">
					<div class="intro-text">
						<div class="intro-heading" style="text-shadow: 2px 2px 5px black">Défi #JeLaLis</div>
						<div class="intro-lead-in" style="border-radius:20px;padding-bottom:30px;color:white;text-shadow: 2px 2px 5px black;">Les autrices choisies et les actions réalisées...</div>
					</div>
				</div>				
			</div>
		</header>
		<section class="overlay-dark bg-img1 dark-bg short-section" id="autrices">
			<div class="container text-center">
				<div class="row">
						<?php
   $sql = "SELECT id_marraine,valide FROM jelalis_marraine  WHERE valide=1;";
   // On envoie la requête :
   $req = $link->prepare($sql);
   $req->execute();
   $nbChoix = $req->rowCount();
   ?>
							<h2 style="color:white;">Les autrices choisies pour le défi #JeLaLis</h2>
				      <br/>
							<center><? echo $nbChoix;?> choix d'autrices enregistrés à ce jour</center>
				</div>
			</div>
		</section>
		<section>
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<div class="skills-text">
<?
   $sql = "SELECT id,autrice,id_autrice,idBnf,idWikidata,image,COUNT(id) AS nb_choix FROM jelalis_marraine,jelalis_autrice WHERE id_autrice=id AND valide=1 AND id_autrice>=0 GROUP BY id_autrice ORDER BY nb_choix DESC, autrice ASC;";
   // On envoie la requête :
   $req = $link->prepare($sql);
   $req->execute();
   echo '						  <h3><br/>'.$req->rowCount().' autrices dans le domaine public</h3><br/>
   ';
   $nb = 0;
   
   // Affichage des autrices
   while($data = $req->fetch()){
      if ($nb == 0){
         echo '<div class="row">';
      }
      $nb += 1;
      
      // Ajout d'un s si choix par plus d'une personne
      $accord = "";
      if($data["nb_choix"]>1){
         $accord = "s";
      }
      
      // Ajout URL
      $url = "";
      if(strlen($data["idBnf"])>0){
         $url = "https://data.bnf.fr/fr/".substr($data["idBnf"],0,strlen($data["idBnf"])-1);
      } else {
         $url = "https://www.wikidata.org/wiki/".$data["idWikidata"];
      }
      
      // Ajout image
      $image = "";
      $extensions = array(".png", ".gif", ".tif", ".PNG");
      if(strlen($data["image"])>0){
         $image = "";
         $image = '<a href="'.$data["image"].'"><img src="'.str_replace("http://commons.wikimedia.org/wiki/Special:FilePath/","http://ledeuxiemetexte.fr/jelalis/autrices/",str_replace($extensions,".jpg",$data["image"])).'" alt="Portrait de '.htmlspecialchars($data["autrice"]).'"></a><br/>';
      };
      
      echo '
								<div class="col-md-3 item text-center">
										<b><a href="'.$url.'">'.htmlspecialchars($data["autrice"]).'</b></a><br/>(choisie par '.$data["nb_choix"].' personne'.$accord.')<br/>
										'.$image.'<br/><br/>
								</div>
      ';
      if ($nb==4){
         $nb=0;
         echo '</div>';
      }
   }
?>
							
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<div class="skills-text">
<?
   $sql = "SELECT autrice,id_autrice,COUNT(autrice) AS nb_choix FROM jelalis_marraine WHERE valide=1 AND id_autrice<0 GROUP BY autrice ORDER BY nb_choix DESC, autrice ASC;";
   // On envoie la requête :
   $req = $link->prepare($sql);
   $req->execute();
   echo '						  <h3>'.$req->rowCount().' autrices pas encore dans le domaine public</h3><br/>
   ';
   $nb = 0;
   while($data = $req->fetch()){
      if ($nb==0){
         echo '<div class="row">';
      }
      $nb+=1;
      $accord="";
      if($data["nb_choix"]>1){
         $accord="s";
      }
      echo '
								<div class="col-md-3 text-center">
										<b><a href="https://www.qwant.com/?q=%22'.htmlspecialchars($data["autrice"]).'%22&t=web">'.htmlspecialchars($data["autrice"]).'</a></b><br/>(choisie par '.$data["nb_choix"].' personne'.$accord.')
										<br/><br/>
								</div>
      ';
      if ($nb==4){
         $nb=0;
         echo '</div>';
      }
   }
?>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
		<section>
			<div id="carte" class="container">
				<div class="row">
					<div class="col-md-12">
						<div class="skills-text">

<?php
   /*
   // Initialisation des liens autrices/lieux dans la base de données
   echo "<ul>";
   $sql = "SELECT jelalis_autrice.id, lieuNaissance, lieuMort, jelalis_lieu.id as id_lieu, lieu FROM jelalis_autrice,jelalis_lieu WHERE lieuMort <> '' AND lieuMort=lieu ORDER BY lieuMort ASC;";
   //$sql = "SELECT jelalis_autrice.id, lieuNaissance, lieuMort, jelalis_lieu.id as id_lieu, lieu FROM jelalis_autrice,jelalis_lieu WHERE lieuNaissance <> '' AND lieuNaissance=lieu ORDER BY lieuNaissance ASC;";
   // On envoie la requête :
   $req = $link->prepare($sql);
   $req->execute();
   while($data = $req->fetch()){
        echo "<li>INSERT INTO `jelalis_lieu_autrice` (id_lieu,id_autrice,type) VALUES (".$data["id_lieu"].",".$data["id"].",'m');</li>";
        // echo "<li>INSERT INTO `jelalis_lieu_autrice` (id_lieu,id_autrice,type) VALUES (".$data["id_lieu"].",".$data["id"].",'n');</li>";
   }
   echo "</ul>";
   */
?>

<?php
// Lancement de la requête pour récupérer les lieux et les autrices liées
   $sql = "SELECT lieu, MAX(type) as max_type, MIN(type) as min_type, jelalis_autrice.nomComplet, jelalis_autrice.id AS ia, jelalis_lieu_autrice.id_lieu as idl, jelalis_lieu_autrice.id_autrice as ida, jelalis_lieu.id as il, jelalis_lieu.latitude, jelalis_lieu.longitude, jelalis_lieu_autrice.type, jelalis_marraine.id_autrice AS choisie FROM jelalis_lieu, jelalis_lieu_autrice,jelalis_autrice LEFT OUTER JOIN jelalis_marraine ON id = jelalis_marraine.id_autrice WHERE jelalis_autrice.id=jelalis_lieu_autrice.id_autrice AND jelalis_lieu.id=jelalis_lieu_autrice.id_lieu GROUP BY jelalis_autrice.id, jelalis_lieu.id ORDER BY lieu ASC,nomComplet ASC";
   // On envoie la requête :
   $req = $link->prepare($sql);
   $req->execute();
   echo '						  <h3><br/><br/><br/>Carte de '.$req->rowCount().' autrices dans le domaine public nées ou mortes en France</h3>Les numéros sur chaque lieu indiquent le nombre d\'autrices liées au lieu qui n\'ont pas encore été choisies pour le défi #JeLaLis.<br/>
   ';
   $nb = 0;
?>

<div id="mapid" style="height: 600px;"></div>
<script type="text/javascript">
// Initialisation de la carte
var map = L.map('mapid').setView([30,0], 2);

L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
    attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
}).addTo(map);

<?php   

// Création des marqueurs par lieu
   $prec_id_lieu = "-1";
   $prec_lieu = "";
   $prec_latitude = "";
   $prec_longitude = "";
   $nees = "";
   $mortes = "";
   $nbNees = 0;
   $nbMortes = 0;
   $nbAutrices = 0;
   $nbAutricesPasChoisies = 0;
   $autricesNonChoisies = array();
   $marqueurs = "";
   // Affichage des autrices
   while($data = $req->fetch()){
      if (intval($data["idl"]) != intval($prec_id_lieu) and ($nbAutrices>0)){
         // Nouveau lieu : création du marqueur avec sa pop-up 
         // contenant la liste des autrices liées au lieu précédent 
         $accord = "";
         if($nbAutrices>1){$accord = "s";}
         if($nbAutrices==1){$nbAutrices = "Une";}
         // Création du marqueur et de la pop-up
         $marqueurs .= 'L.marker(['.$prec_latitude.','.$prec_longitude.'], {icon: marker'.$nbAutricesPasChoisies.'}).addTo(map).bindPopup("<center><b>'.htmlspecialchars($prec_lieu).'</b></center>'.$nbAutrices.' autrice'.$accord.' liée'.$accord.' à ce lieu' ;
         if($nbAutricesPasChoisies>1){$accord = "s";}
         if($nbAutricesPasChoisies==1){$formulation = "";}else{$formulation = 'dont '.$nbAutricesPasChoisies.' ';}
         $marqueurs .= ' ('.$formulation .'pas encore choisie'.$accord.' pour le défi #JeLaLis)';
         // Liste des autrices nées dans le lieu
         if($nbNees>0){
            $accord = "";
            if($nbNees>1){$accord="s";}
            $marqueurs .= '<br/>Née'.$accord.' à '.htmlspecialchars($prec_lieu).' : '.$nees.'.';
         }
         // Liste des autrices mortes dans le lieu
         if($nbMortes>0){
            $accord = "";
            if($nbMortes>1){$accord="s";}
            $marqueurs .= '<br/>Morte'.$accord.' à '.htmlspecialchars($prec_lieu).' : '.$mortes.'.';
         }
         $marqueurs .= '");
';
         
         // Ajout (s'il n'y est pas déjà) du nombre d'autrices du lieu 
         // dans le tableau permettant de créer la liste des icônes de marqueurs avec un numéro
         if(!(array_key_exists($autricesNonChoisies, $nbAutricesPasChoisies))){
            $autricesNonChoisies[$nbAutricesPasChoisies] = 1;
         }
         
         // Remise à zéro des infos sur les autrices liées au lieu
         $nbAutrices = 0;
         $nbAutricesPasChoisies = 0;
         $nees = '';
         $mortes = '';
         $nbNees = 0;
         $nbMortes = 0;
      }

      // Si l'autrice est née dans le lieu,  on l'ajoute à la liste des autrices nées dans le lieu
      if ($data["max_type"]=="n"){
         $nbNees += 1;
         if($nees != ""){
            $nees .= ", ";
         }
         if (is_null($data["choisie"])){
            $nees .= '<a href=\\"index.php?autrice='.urlencode(htmlspecialchars($data["nomComplet"])).'#inscription\\">'.htmlspecialchars($data["nomComplet"]).'</a>';
         } else {
            $nees .= htmlspecialchars($data["nomComplet"]);
         }
      }

      // Si l'autrice est morte dans le lieu, on l'ajoute à la liste des autrices mortes dans le lieu
      if ($data["min_type"]=="m"){
         $nbMortes += 1;
         if($mortes != ""){
            $mortes .= ", ";
         }
         if (is_null($data["choisie"])){
            $mortes .= '<a href=\\"index.php?autrice='.urlencode(htmlspecialchars($data["nomComplet"])).'#inscription\\">'.htmlspecialchars($data["nomComplet"]).'</a>';
         } else {
            $mortes .= htmlspecialchars($data["nomComplet"]);   
         }
      }
      
      // Mise à jour du nombre d'autrices liées au lieu
      $nbAutrices += 1;
      
      // Mise à jour du nombre d'autrices liées au lieu pas encore choisies pour le défi #JeLaLIs
      if (is_null($data["choisie"])){
         $nbAutricesPasChoisies += 1;
      }

      // Enregistrement des caractéristiques du lieu (pour les ajouter plus tard dans la pop-up)
      $prec_id_lieu = $data["idl"];
      $prec_lieu = $data["lieu"];
      $prec_latitude = $data["latitude"];
      $prec_longitude = $data["longitude"];
   }
   
   // Création des icônes de marqueurs indiquant le nombre d'autrices pas encore choisies
   foreach($autricesNonChoisies as $i => $nbAutrices){
      $color = "blue";
      // Marqueur vert si entre 5 et 9 autrices n'ont pas encore été choisies
      if($i>4){$color = "green";}
      // Marqueur rouge si plus de 9 autrices n'ont pas encore été choisies
      if($i>9){$color = "red";}
      echo  "
      var marker".$i." = L.ExtraMarkers.icon({
      icon: 'fa-number',
      number: ".$i.",
      markerColor: '".$color."',
      shape: 'square',
      prefix: 'fa'
      });" ;
   }
   
   // Affichage de la liste des marqueurs
   echo ($marqueurs);
   
?>

</script>

							</div>
						</div>
					</div>
				</div>
			</div>
		</section>

		<section class="overlay-dark bg-img1 dark-bg short-section" id="actions">
			<div class="container text-center">
				<div class="row">
						<?php
   $sql = "SELECT id_action FROM jelalis_action WHERE 1;";
   // On envoie la requête :
   $req = $link->prepare($sql);
   $req->execute();
   $nbChoix = $req->rowCount();
   ?>
							<h2 style="color:white;">Les actions réalisées pour le défi #JeLaLis</h2>
				      <br/>
							<center><? echo $nbChoix;?> actions recensées à ce jour</center>
				</div>
			</div>
		</section>
		<section>
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<div class="skills-text">
						  <ul>
<?
   $sql = "SELECT * FROM jelalis_action WHERE 1 ORDER BY DATE DESC;";
   // On envoie la requête :
   $req = $link->prepare($sql);
   $req->execute();
   $nb = 0;
   $previousDate = "";
   
   // Affichage des actions
   while($data = $req->fetch()){
      if($nb == 0){
         echo '<li>'.$data["date"].'<ul>';
      } else {
         if($data["date"] != $previousDate){
            echo '</ul></li><li>'.$data["date"].'<ul>';
         }
      }
      
      $reseau = $data["reseau"];
      $iconeReseau = "";
      if($reseau == 'Blog'){
         $iconeReseau = ' <i class="fas fa-blog"></i> ';
         $reseau = '';
      } elseif ($reseau == 'Facebook'){
         $iconeReseau = ' <i class="fab fa-facebook"></i> ';
         $reseau = '';
      } elseif ($reseau == 'Twitter'){
         $iconeReseau = ' <i class="fab fa-twitter"></i> ';
         $reseau = '';
      } elseif ($reseau == 'YouTube'){
         $iconeReseau = ' <i class="fab fa-youtube"></i> ';
         $reseau = '';
      } elseif ($reseau == 'Wikipédia'){
         $iconeReseau = ' <i class="fab fa-wikipedia-w"></i> ';
         $reseau = '';
      } elseif ($reseau == 'Wikisource'){
         $iconeReseau = ' <i class="fas fa-book-open"></i> ';
         $reseau = '';
      } elseif ($reseau == 'LinkedIn'){
         $iconeReseau = ' <i class="fab fa-linkedin"></i> ';
         $reseau = '';
      } elseif ($reseau == 'Instagram'){
         $iconeReseau = ' <i class="fab fa-instagram"></i> ';
         $reseau = '';
      } elseif ($reseau == 'Site web'){
         $iconeReseau = ' <i class="far fa-window-restore"></i> ';
         $reseau = '';
      } else {
         $reseau = " (".$reseau.")";
      }
      
      echo '<li><a href="'.$data["url"].'">'.$iconeReseau.'<b>'.$data["type"].'</b></a>'.$reseau.', sur <b>'.$data["autrice_action"].'</b></li>';
      $previousDate = $data["date"];
      $nb += 1;
   }
?>
              </ul>
						</div>
					</div>
				</div>
			</div>
		</section>
		<section class="overlay-dark bg-img1 dark-bg short-section" id="faq">
			<div class="container text-center">
				<div class="row">
				  <h2 style="color:white;">Foire aux questions</h2>
				  <br/>
				  <center>
Une question non posée ci-dessous ?
Posez-la <a href="https://mypads.framapad.org/mypads/?/mypads/group/hackegalitefh-xtd5k7dq/pad/view/jelalis-faq-collaborative-592z4k775">sur ce document collaboratif</a> !
          </center>
				</div>
			</div>
		</section>
		<section>
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<div class="skills-text">

<h3>1. J'ai du mal à comprendre ce que je dois faire. Pouvez vous me donner des pistes ?</h3>

<p>
L'idée du défi est de redonner de la visibilité à l'autrice que vous avez choisie de la manière la plus inventive possible. Nous vous suggérons donc quelques pistes d'actions ci-dessous, à vous de voir lesquelles vous semblent les plus intéressantes, ou si ces suggestions vous inspirent d'autres idées !
</p>

<h4><br/>Vous renseigner sur l'autrice choisie</h4>

<p>
La première étape est peut-être de partir à la rencontre de l'autrice que vous avez choisie, en vous renseignant par exemple sur la Wikipédia ou sur des articles ou ouvrages écrits à son sujet. Pour cela, il peut être utile de faire des recherches sur son nom sur un moteur de recherche généraliste. Vous pouvez aussi utiliser le moteur de recherche de <a href="http://gallica.bnf.fr">Gallica</a> (avec la <a href="https://gallica.bnf.fr/services/engine/search/advancedSearch/?lang=fr">recherche avancée</a>, si vous ciblez l'année de son décès, vous accéderez peut-être à des nécrologies intéressantes), afin d'accéder à des articles de presse ancienne qui la concernent, ou bien sur <a href="http://books.google.com">Google Books</a> ou <a href="http://scholar.google.com">Google Scholar</a> dans le cas où des livres ou des articles scientifiques parleraient d'elles.
</p>

<p>
Il est possible de commencer à communiquer pendant cette étape, pour annoncer sur les réseaux sociaux que vous avez choisi de marrainer cette femme de lettres, en donnant envie d'en découvrir davantage sur sa vie.
</p>

<p>
Exemples d'annonces de choix d'une autrice :
<ul>
<li><a href="https://twitter.com/LeRoyGwladys/status/1115178712033636352">Isabelle Eberhardt par Gwladys</a>, sur Twitter ;</li>
<li><a href="https://www.facebook.com/photo.php?fbid=10216345829346099&amp;set=p.10216345829346099&amp;type=3&amp;theater">Claire de Duras par Marta</a>, sur Facebook</li>
</ul>
</p>


<h4><br/>Dessiner un portrait de votre autrice</h4>

<p>
Si vous avez des talents picturaux, lancez-vous dans un portrait ou une caricature de votre autrice. Si vous partagez vos dessins ou aquarelles sous la <a href="https://creativecommons.org/licenses/by-sa/4.0/deed.fr">licence libre Creative Commons BY-SA 4.0</a>, par exemple, ils pourront aussi être repris dans Wikipédia si on n'y trouve aucun portrait de l'écrivaine.
</p>

<p>
Exemples de dessins d'écrivaines :
<ul>
<li>les portraits d'autrices pour <a href="http://diglee.com/poetesses-et-inktober-2017/">Inktober 2017</a> et <a href="http://diglee.com/inktober-2017/">2018</a> par Diglee ;</li>
<li>la <a href="https://blequin.blog4ever.com/il-y-a-cent-ans-mourait-marie-leneru">caricature de Marie Lenéru</a> pour le centenaire de sa mort, par Blequin ;</li>
<li>la <a href="https://www.casterman.com/Bande-dessinee/Catalogue/ecritures/olympe-de-gouges">bande dessinée <i>Olympe de Gouges</i></a> par Catel et Bocquet.</li>
</ul>
</p>


<h4><br/>Partager vos découvertes</h4>

<p>
Créer ou compléter la page Wikipédia de l'autrice choisie peut être une bonne façon de partager vos découvertes à son sujet. Si vous n'avez jamais contribué à la Wikipédia, demandez-nous par mail et nous pourrons vous aider ! Un principe essentiel est que toute information que vous ajoutez à Wikipédia doit avoir une source clairement indiquée, donc pensez à noter l'adresse de chaque page web où vous trouvez une information.
</p>

<p>
Exemples de partages de découvertes :
<ul>
<li><a href="https://twitter.com/tatakDH/status/1116759194340716544">annonce sur Twitter</a> de la création de la page Wikipédia de Juliette Figuier par Tata k</li>
<li><a href="https://fr.wikipedia.org/w/index.php?title=Isabelle_Eberhardt&amp;type=revision&amp;diff=158327494&amp;oldid=158165638">ajout dans la Wikipédia</a> de deux pseudonymes d'Isabelle Eberhardt ;</li>
<li>contact de la Bibliothèque nationale de France pour demander la fusion de <a href="https://catalogue.bnf.fr/ark:/12148/cb12368913z">deux</a> <a href="https://catalogue.bnf.fr/ark:/12148/cb12274993h">pages</a> du catalogue qui concernaient la même écrivaine, Nisia Floresta, ou pour ajouter <a href="https://catalogue.bnf.fr/ark:/12148/cb10699488k.public">la date de décès de Berthe Rey alias Tony d'Ulmès</a>.</li>
</ul>
</p>


<h4><br/>Vous procurer ses ouvrages</h4>

<p>
Allez chez votre libraire, ou à la bibliothèque ou médiathèque locale, pour vous procurer un des ouvrages de votre autrice. S'ils sont difficiles à trouver, vous pouvez essayer les sites web de vente de livres d'occasion, le catalogue des bibliothèques universitaires (<a href="http://www.sudoc.abes.fr">sudoc.abes.fr</a> puis lors de la consultation de la notice d'un ouvrage, suivre le lien "Où trouver ce document"), ceux de la Bibliothèque nationale de France (<a href="http://catalogue.bnf.fr">catalogue.bnf.fr</a> pour des ouvrages publiés en France et <a href="https://archivesetmanuscrits.bnf.fr/">archivesetmanuscrits.bnf.fr</a> pour des manuscrits conservés par la BnF) ou encore des bibliothèques d'ouvrages électroniques comme <a href="http://archive.org">The Internet Archive</a>, <a href="http://gutenberg.org">le projet Gutenberg</a> ou <a href="http://fr.wikisource.org">Wikisource</a>.
</p>

<p>
Vous pouvez alors prendre en photo les ouvrages trouvés, éventuellement en vous mettant en scène sur la photo.
</p>

<p>
Exemples de photos d'ouvrages :
<ul>
<li>achat de <i>Le Prix Lacombyne</i> de Renée Dunan par Alphonsine, <a href="https://www.instagram.com/p/BwUKx7zH2GE/">photo sur Instagram</a> ;</li>
<li>lecture de pièces de théâtre de Marie Lenéru par Philippe sur la plage, <a href="https://twitter.com/Georgele2etexte/status/1103772392265007105">photo sur Twitter</a> pour le défi #7jour7livres de l'association Le deuxième texte.</li>
</ul>
</p>


<h4><br/>Lire et faire connaître ses ouvrages</h4>

<p>
Suite à vos lectures, vous pouvez partager des impressions ou notes de lecture pour donner envie à d'autres personnes de se plonger dedans. Si possibles, pensez à ajouter des illustrations pour rendre vos textes attractifs.
</p>

<p>
Exemples de notes de lecture :
<ul>
<li><a href="https://twitter.com/Liueruce/status/1117194720780922884">fil de tweets de Liueruce</a> à propos des <i>Poèmes de la Libellule</i>, traduits par Judith Gautier depuis le japonais ;</li>
<li><a href="https://www.facebook.com/emmanuelle.cordoliani/posts/2320372678015421">impressions de lecture d'Emmanuelle</a> à propos d'Isabelle de Charrière ;</li>
<li><a href="https://bibliothequealphonsine.wordpress.com/2019/04/15/jelalis-renee-dunan-et-le-stylet-en-langue-de-carpe/">billet de blog d'Alphonsine</a> à propos du roman <i>Le Stylet en langue de carpe</i> de Renée Dunan.</li>
</ul>
</p>


<h4><br/>Extraire et partager des citations de ses ouvrages</h4>

<p>
Si au gré de vos lectures, vous notez des phrases qui vous font sourire, ou que vous trouvez pleine de finesse, notez et partagez ces citations.
</p>

<p>
Exemples de partages de citations :
<ul>
<li>une <a href="https://twitter.com/ConstanceDrd/status/1116405013134413824">citation de Marie d'Agoult en lien avec l'actualité politique</a>, proposée par Constance sur Twitter ;</li>
<li>l'<a href="https://fr.wikiquote.org/w/index.php?title=Marie_d%27Agoult&amp;type=revision&amp;diff=276788&amp;oldid=259690">ajout sur Wikiquote</a> d'une autre citation de Marie d'Agoult repérée par Constance.</li>
</ul>
</p>


<h4><br/>Mettre en ligne ses ouvrages</h4>

<p>
Si l'autrice que vous avez choisie est dans le domaine public (en général, décédée depuis plus de 70 ans) vous pouvez partager les ouvrages qu'elle publiés de son vivant (en ce qui concerne les manuscrits, c'est plus compliqué, voir par exemple <a href="https://cahier.hypotheses.org/corpus-dauteurs-aspects-juridiques-manuscrits-d-oeuvre">ces précisions juridiques</a>). Vous pouvez par exemple trouver des versions scannées de tels ouvrages dans le domaine public sur les sites <a href="http://gallica.bnf.fr">Gallica</a>, par la Bibliothèque nationale de France), <a href="http://archive.org">The Internet Archive</a> ou <a href="http://books.google.com">Google Books</a>.
</p>

<p>
Afin de rendre l'ouvrage davantage accessible, ou lisible avec un bon confort de lecture sur diverses tailles d'écran, il peut être utile de mettre à disposition le texte intégral de ces ouvrages. Ceci est possible sur des sites comme <a href="http://fr.wikisource.org">la Wikisource francophone</a> où des bénévoles corrigent le texte des ouvrages obtenu par reconnaissance automatique de caractères. Si vous souhaitez savoir comment contribuer au site Wikisource, n'hésitez pas à vous joindre à l'un de nos ateliers Wikisource autrices (<a href="https://fr.wikisource.org/wiki/Wikisource:Autrices">liste des derniers et prochains ateliers</a>) ou nous solliciter pour en organiser un près de chez vous.
</p>

<p>
Exemples de mise en ligne d'ouvrages :
<ul>
<li>sur Wikimédia Commons, mise en ligne par l'association Le deuxième texte d'<a href="https://commons.wikimedia.org/wiki/File:Len%C3%A9ru_-_La_Triomphatrice.djvu?uselang=fr">une version scannée de la pièce <i>La Triomphatrice</i> de Marie Lenéru</a></li>
<li>sur Wikisource, contribution par l'association Le deuxième texte, depuis 2017, à la mise en ligne de <a href="https://fr.wikisource.org/wiki/Utilisateur:George2etexte">plus de 50 ouvrages écrits par des femmes</a></li>
</ul>
</p>


<h4><br/>Faire jouer ses pièces de théâtre</h4>

<p>
Vous avez choisi une dramaturge ? Seules 17% des pièces de théâtre représentées dans les théâtres nationaux en France étaient écrites par des femmes en 2017-2018, d'après l'<a href="http://www.culture.gouv.fr/Thematiques/Etudes-et-statistiques/L-actualite-du-DEPS/Observatoire-2019-de-l-egalite-entre-femmes-et-hommes-dans-la-culture-et-la-communication"><i>Observatoire 2019 de l'égalité entre femmes et hommes dans la culture et la communication</i></a>. Faire représenter une pièce de votre autrice permettra de rendre ce texte vivant, en le faisant connaître et porter par toute une troupe.
</p>

<p>
Exemples :
<ul>
<li>le musée Marguerite Audoux <a href="https://www.leberry.fr/sainte-montaine-18700/loisirs/le-musee-marguerite-audoux-a-un-projet-fou-et-cherche-des-acteurs-amateurs_13044809/">est en train de monter une pièce sur la vie de l'autrice</a>, inspirée du livre <i>L'atelier de Marie-Claire</i> ;</li>
<li>Aurore Evain a mis en scène la pièce de Marie-Catherine de Villedieu <i>Le Favori</i>, <a href="https://www.epeedebois.com/un-spectacle/le-favori/">jouée par la compagnie La Subversive du 9 au 19 mai au Théâtre de l'Epée de Bois</a> ;</li>
<li>la pièce <i>Zamore et Mirza</i>, ou l'heureux naufrage d'Olympe de Gouges avait été <a href="http://nagoya-en-francais.over-blog.com/article-zamore-et-mirza-113732444.html">mise en scène et jouée en 2012 par des étudiantes et étudiants de l'université catholique Nanzan de Nagoya</a>.
</li>
</ul>
</p>


<h4><br/>Ajouter son portrait ou sa signature dans Wikipédia</h4>

<p>
Difficile de rendre visible une autrice quand on ne dispose pas d'illustration pour la représenter. Un portrait ou une signature, stylisée, sont des éléments utiles pour communiquer en attirant l'attention. La presse ancienne disponible sur Gallica, ou la presse locale ancienne référencée sur <a href="http://presselocaleancienne.bnf.fr/accueil">http://presselocaleancienne.bnf.fr</a> peuvent constituer des ressources utiles pour trouver des portraits. Quant aux signatures, on peut les trouver sur des sites web de vente aux enchères ou de libraires de livres anciens en soumettant aux moteurs de recherche le nom de son autrice accompagné de "signature", "autographe" ou "manuscrit".
</p>

<p>
Pour ajouter une signature ou un portrait sur Wikipédia, il faut en fait : 1) passer par le site Wikimédia Commons pour mettre en ligne le fichier image contenant le portrait ou la signature, en respectant les contraintes nécessitant que les contenus soumis soient dans le domaine public ou sous licence libre ; 2) aller sur la page de l'autrice sur Wikidata pour y "ajouter une déclaration", en choisissant une déclaration de type "illustration" ou "signature", puis en indiquant l'adresse du fichier chargé sur Wikimédia Commons ; 3) si le portrait ou la signature n'apparaît pas dans l'"infobox", c'est-à-dire le cadre en haut à droite de la page Wikipédia de l'autrice, modifier cette infobox pour que l'image apparaisse, soit en complétant le champ voulu pour indiquer le nom de fichier de l'image du portrait ou de la signature, soit en utilisant le modèle Biographie2 qui va chercher automatiquement les informations dans Wikidata.
</p>

<p>
Exemples :
<ul>
<li>les contributrices Wikipédia AwkwardChester et Anne-LaureM <a href="https://george2etexte.wordpress.com/2019/06/09/signatures-de-femmes-de-lettres-2/">ont chargé sur Wikipédia plusieurs signatures de femmes de lettres</a> tirées de la base Léonore des récipiendaires de la légion d'honneur ;</li>
<li>l'équipe de coordination de #JeLaLis a tout particulièrement cherché des portraits des autrices choisies au moins deux fois pour le défi ; elle a ainsi pu trouver des portraits de Laure Surville et Marie-Jeanne Riccoboni déposés dans Wikimédia Commons ; le portrait de Marie Colmont trouvé <a href="https://gallica.bnf.fr/ark:/12148/bpt6k76513848/f3">dans <i>Comoedia</i></a> n'a en revanche pas pu être déposé, en raison des contraintes de Wikimédia Commons sur les contenus qui ne sont pas dans le domaine public aux Etats-Unis.</li>
</ul>
</p>


<h4><br/>Faire travailler ses élèves sur l'autrice choisie</h4>

<p>
Nous donnons <a href="http://ledeuxiemetexte.fr/jelalis/pdf/activites_pedagogiques-JeLaLis.pdf">plusieurs idées d'activités pédagogiques dans ce document</a>.
</p>


<h3>2. Est-il possible de participer au défi pendant l'année scolaire 2019/2020 ?</h3>

<p>
Cela devrait être possible si vous le prévoyez en début d'année scolaire, car nous prévoyons une remise des prix en décembre (et donc probablement une échéance fin octobre pour y participer et laisser ensuite le temps au jury de faire son choix). Nous n'avons pas encore arrêté le calendrier précis mais cela sera fait d'ici le mois de juin.
</p>


<h3>3. Y a-t-il une date limite pour participer au concours ? Quelles sont les conditions pour y participer ?</h3>

<p>
Nous n'avons pas encore arrêté le calendrier précis mais cela sera fait d'ici le mois de juin. Nous prévoyons une remise des prix en décembre, donc il est probable que l'échéance pour participer au concours sera fin octobre.
</p>


<h3>4. Peut-on participer au concours si l'on n'habite pas en France ?</h3>

<p>
Le défi est ouvert à l'international, car nous avons constaté un manque de visibilité des femmes de lettres dans l'histoire littéraire de l'ensemble du monde francophone.
Ceci dit, la remise des prix aura lieu <!--à l'Espace des femmes -->à Paris,<!-- grâce à notre partenaire les éditions des femmes - Antoinette Fouque,--> mais nous les enverrons par voie postale si les lauréates ou lauréats ne peuvent assister en personne à la remise des prix.
</p>


<h3>5. Peut-on changer d'autrice si on ne trouve pas d'information à son sujet ?</h3>

<p>
En effet, le manque d'informations sur certaines autrices peut parfois être bloquant pour leur donner de la visibilité, vous pouvez donc tout à fait changer d'autrice.
</p>

<p>
Pour cela vous pouvez soit vous inscrire de nouveau sur le site (et comme vous nous en avez informé nous supprimerons votre précédente inscription), soit nous indiquer <a href="#contact">par courriel</a> quelle femme de lettres vous aimeriez choisir !
</p>

						</div>
					</div>
				</div>
			</div>
		</section>

		<section id="contact">
			<div class="container">
				<div class="row">
					<div class="col-lg-12 text-center">
						<div class="section-title">
							<h2>CONTACT</h2>
							<p>Contactez-nous à l'adresse <a href="mailto:jelalis@ledeuxiemetexte.fr">jelalis@ledeuxiemetexte.fr</a> pour toute question sur cette opération.</p>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-lg-12 text-center">
						<div class="section-title">
							<h3>Sources des illustrations</h3>
							<p>
							Tableau <i>Les éclaireuses</i> d'Amélie Beaury-Saurel, exposé au Salon de Paris en 1914<br/>
							(source&nbsp;: <a href="https://bibliotheques-specialisees.paris.fr/ark:/73873/pf0000858991">Ville de Paris / Bibliothèque Marguerite&nbsp;Durand</a>).
							<br/><br/>
							Informations relatives aux autrices et portraits extraits de leur fiche<br/>sur <a href="http://data.bnf.fr">data.bnf</a> (Bibliothèque nationale de France, <a href="https://data.bnf.fr/sparql?default-graph-uri=&query=PREFIX+foaf%3A+%3Chttp%3A%2F%2Fxmlns.com%2Ffoaf%2F0.1%2F%3E%0D%0APREFIX+rdarelationships%3A+%3Chttp%3A%2F%2Frdvocab.info%2FRDARelationshipsWEMI%2F%3E%0D%0APREFIX+dcterms%3A+%3Chttp%3A%2F%2Fpurl.org%2Fdc%2Fterms%2F%3E%0D%0APREFIX+bio%3A+%3Chttp%3A%2F%2Fvocab.org%2Fbio%2F0.1%2F%3E%0D%0APREFIX+bnf-onto%3A+%3Chttp%3A%2F%2Fdata.bnf.fr%2Fontology%2Fbnf-onto%2F%3E%0D%0APREFIX+skos%3A+%3Chttp%3A%2F%2Fwww.w3.org%2F2004%2F02%2Fskos%2Fcore%23%3E%0D%0APREFIX+rdagroup2elements%3A+%3Chttp%3A%2F%2Frdvocab.info%2FElementsGr2%2F%3E%0D%0ASELECT+DISTINCT+%3Fautrice+%3FnomComplet+%3FlieuNaissance+%3FlieuMort+%28MIN%28%3Fim%29+AS+%3Fimage%29+%3Fprenom+%3Fisni+%3Fnom+%3Fnaissance+%3Fmort++WHERE+%7B%0D%0A++%3Fexpr+dcterms%3Acontributor+%3Fautrice+.%0D%0A++%3Fexpr+%3Chttp%3A%2F%2Fdata.bnf.fr%2Fvocabulary%2Froles%2Fr70%3E+%3Fautrice.%0D%0A++%3Fman+rdarelationships%3AexpressionManifested+%3Fexpr.%0D%0A++%3Fman+dcterms%3Adate+%3Fdate.%0D%0A++FILTER%28%3Fdate%3C%221949%22%29.%0D%0A++%3Fexpr+dcterms%3Atype+%3Chttp%3A%2F%2Fpurl.org%2Fdc%2Fdcmitype%2FText%3E.%0D%0A++%3Fexpr+dcterms%3Alanguage+%3Chttp%3A%2F%2Fid.loc.gov%2Fvocabulary%2Fiso639-2%2Ffre%3E.%0D%0A++%3Fautrice+bio%3Adeath+%3Fmort.%0D%0A++OPTIONAL%7B%3Fautrice+bio%3Abirth+%3Fnaissance.%7D%0D%0A++%3Fautrice+foaf%3Agender+%22female%22.%0D%0A++OPTIONAL%7B%3Fautrice+foaf%3Adepiction+%3Fim.%7D%0D%0A++OPTIONAL%7B%3Fautrice+rdagroup2elements%3AplaceOfBirth+%3FlieuNaissance.%7D+%0D%0A++OPTIONAL%7B%3Fautrice+rdagroup2elements%3AplaceOfDeath+%3FlieuMort%7D.%0D%0A++FILTER%28STRLEN%28%3Fmort%29+%3E%3D+4+%26%26+SUBSTR%28%3Fmort%2C1%2C4%29%3C%221949%22+%26%26+%3Fmort+%21%3D+%2219..%22%29%0D%0A++OPTIONAL+%7B%0D%0A++++%3FcIsni+foaf%3Afocus+%3Fautrice+.%0D%0A++++%3FcIsni+isni%3AidentifierValid+%3Fisni+.%0D%0A++%7D%0D%0A++OPTIONAL+%7B%3Fautrice+foaf%3Aname+%3FnomComplet+.%7D%0D%0A++OPTIONAL+%7B%3Fautrice+foaf%3AfamilyName+%3Fnom+.%7D%0D%0A++OPTIONAL+%7B%3Fautrice+foaf%3AgivenName+%3Fprenom+.%7D%0D%0A++OPTIONAL+%7B%3Fautrice+foaf%3AlanguageOfThePerson+%3Flang+.%7D%0D%0A%7D+GROUP+BY+%3Fautrice+%3FnomComplet+%3Fisni+%3Fprenom+%3Fnom+%3Fmort+%3Fnaissance+%3FlieuNaissance+%3FlieuMort%0D%0AORDER+BY+DESC%28%3Fnaissance%29&format=text%2Fhtml&timeout=0&should-sponge=&debug=on">requête utilisée</a>) ou sur <a href="http://wikidata.org">Wikidata</a> (<a href="https://query.wikidata.org/embed.html#%23Autrices%20de%20langue%20fran%C3%A7aises%20mortes%20avant%201949%0ASELECT%20%3Fautrice%20%3FautriceLabel%20%3FlieuNaissance%20%3FlieuNaissanceLabel%20%3FlieuMort%20%3FlieuMortLabel%20%28MIN%28%3Fimg%29%20AS%20%3Fimage%29%20%3FprenomLabel%20%28MIN%28%3FidIsni%29%20as%20%3Fisni%29%20%3FidBnf%20%28MIN%28%3Fn%29%20AS%20%3Fnaissance%29%20%28MAX%28%3Fm%29%20AS%20%3Fmort%29%20WHERE%20%7B%0A%20%20%3Fautrice%20wdt%3AP106%20wd%3AQ36180.%0A%20%20%3Fautrice%20wdt%3AP21%20wd%3AQ6581072.%0A%20%20%3Fautrice%20wdt%3AP570%20%3Fm.%0A%20%20%7B%20%3Fautrice%20wdt%3AP1412%20wd%3AQ150.%20%7D%0A%20%20UNION%0A%20%20%7B%20%3Fautrice%20wdt%3AP1412%20wd%3AQ1473289.%20%7D%0A%20%20OPTIONAL%20%7B%20%3Fautrice%20wdt%3AP569%20%3Fn.%7D%0A%20%20OPTIONAL%20%7B%20%3Fautrice%20wdt%3AP19%20%3FlieuNaissance.%7D%0A%20%20OPTIONAL%20%7B%20%3Fautrice%20wdt%3AP20%20%3FlieuMort.%7D%0A%20%20OPTIONAL%20%7B%20%3Fautrice%20wdt%3AP18%20%3Fimg.%20%7D%0A%20%20OPTIONAL%20%7B%20%3Fautrice%20wdt%3AP735%20%3Fprenom.%20%7D%0A%20%20OPTIONAL%20%7B%20%3Fautrice%20wdt%3AP213%20%3FidIsni.%20%7D%0A%20%20OPTIONAL%20%7B%20%3Fautrice%20wdt%3AP268%20%3FidBnf.%20%7D%0A%20%20SERVICE%20wikibase%3Alabel%20%7B%20bd%3AserviceParam%20wikibase%3Alanguage%20%22%5BAUTO_LANGUAGE%5D%2Cfr%2Cen%22.%20%7D%0A%20%20FILTER%28%28YEAR%28%3Fm%29%29%20%3C%201949%29%0A%7D%0AGROUP%20BY%20%3Fautrice%20%3FautriceLabel%20%3FlieuNaissance%20%3FlieuNaissanceLabel%20%3FlieuMort%20%3FlieuMortLabel%20%3Fimage%20%3FprenomLabel%20%3Fisni%20%3FidBnf%20%3Fmort%20%3Fnaissance%0AORDER%20BY%20DESC%28%3Fnaissance%29">requête utilisée</a>).
							<br/><br/>
							Icônes tirées de <i><a href="http://fontawesome.com">FontAwesome</a></i>
							</p>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-lg-12 text-center">
						<div class="section-title">
							<h3>À propos du défi #JeLaLis…</h3>
							<p>
							Rendez-vous <a href="http://jelalis.ledeuxiemetexte.fr">sur la page principale de ce site web</a> pour en savoir plus sur le défi #JeLaLis !
							</p>
						</div>
					</div>
				</div>
			</div>
		</section>
		<p id="back-top">
			<a href="#top"><i class="fa fa-angle-up"></i></a>
		</p>
		<footer>
			<div class="container text-center">
				<p>Thème conçu par <a href="http://moozthemes.com"><span>MOOZ</span>Themes.com</a></p>
			</div>
		</footer>


		<!-- Bootstrap core JavaScript
			================================================== -->
		<!-- Placed at the end of the document so the pages load faster -->
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
		<script src="http://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
		<script src="js/bootstrap.min.js"></script>
		<script src="js/owl.carousel.min.js"></script>
		<script src="js/cbpAnimatedHeader.js"></script>
		<script src="js/theme-scripts.js"></script>
		<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
		<script src="js/ie10-viewport-bug-workaround.js"></script>
		<script>
			$(document).ready(function(){
			})
		
		</script>
	</body>
</html>