<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
		<meta name="description" content="">
		<meta name="author" content="">
		<link rel="icon" href="favicon.ico">
		<title>Défi "Je la lis" ! Pour remettre les femmes de lettres à leur place : #JeLaLis</title>
		<!-- Bootstrap core CSS -->
		<link href="css/bootstrap.min.css" rel="stylesheet">
		<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.1/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
		<link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/themes/smoothness/jquery-ui.css">
		<!-- Custom styles for this template -->
		<link href="css/owl.carousel.css" rel="stylesheet">
		<link href="css/owl.theme.default.min.css"  rel="stylesheet">
		<link href="css/style.css" rel="stylesheet">
		<!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
		<!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
		<script src="js/ie-emulation-modes-warning.js"></script>
		<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		<![endif]-->
	</head>
<?php

include("connect.php");

?>
	<body id="page-top">
	<!--
	<div style="background-color:rgba(0,0,0,0.5);position:fixed;left:0px;bottom:0px;right:0px;height:110px;z-index:1000;text-align:center;padding:20px;color:white;text-shadow: 2px 2px 5px black;font-size:1.3em;">
	<b>SITE DE D&Eacute;MONSTRATION, CONTENU NON D&Eacute;FINITIF.</b><br/>
	<span style="color:red;"><b>DÉBUT DE L'OPÉRATION LE  2 AVRIL 2019.<br/>
	MERCI DE NE PAS LA DIFFUSER AVANT CE JOUR.</b></span><br/>
	</div>
	-->
		<!-- Navigation -->
		<nav class="navbar navbar-default navbar-fixed-top">
			<div class="container">
				<!-- Brand and toggle get grouped for better mobile display -->
				<div class="navbar-header page-scroll" style="padding-top:15px">
					<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					</button>
					<a class="navbar-brand page-scroll" href="#page-top">
					<span style="color:#c71E17;font-family:'Open Sans',sans-serif;">
					<b>JE LA LIS !</b>
					</span>
					</a>
				</div>
				<!-- Collect the nav links, forms, and other content for toggling -->
				<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
					<ul class="nav navbar-nav navbar-right">
						<li class="hidden">
							<a href="#page-top"></a>
						</li>
						<li>
							<a class="page-scroll" href="#about">Le concept</a>
						</li>
						<li>
							<a class="page-scroll" href="#inscription">Je participe !</a>
						</li>
						<li>
							<a class="page-scroll" href="#autrices">Autrices choisies</a>
						</li>
						<li>
							<a class="page-scroll" href="#team">Partenaires</a>
						</li>
						<li>
							<a class="page-scroll" href="#contact">Contact</a>
						</li>
					</ul>
				</div>
				<!-- /.navbar-collapse -->
			</div>
			<!-- /.container-fluid -->
		</nav>
		<!-- Header -->
		<header>
			<div class="container">
				<div class="slider-container">
					<div class="intro-text">
						<div class="intro-heading" style="text-shadow: 2px 2px 5px black">Je la lis !</div>
						<div class="intro-lead-in" style="border-radius:20px;padding-bottom:30px;color:white;text-shadow: 2px 2px 5px black;">Pour remettre les femmes de lettres à leur place</div>
						<a href="#about" class="page-scroll btn btn-xl" style="box-shadow: 2px 2px 5px black;">#JeLaLis</a>
					</div>
				</div>
			</div>
		</header>
		<section id="about" class="light-bg">
			<div class="container">
				<div class="row">
					<div class="col-lg-12 text-center">
						<div class="section-title">
							<h2>LE CONCEPT</h2>
						</div>
					</div>
				</div>
				<div class="row">
					<!-- about module -->
					<div class="col-md-3 text-center">
						<div class="mz-module-about">
							<i class="fa fa-feather-alt ot-circle"></i>
							<h3>Je choisis<br/> une autrice</h3>
						</div>
					</div>
					<!-- end about module -->
					<!-- about module -->
					<div class="col-md-3 text-center">
						<div class="mz-module-about">
							<i class="fa fa-book-reader ot-circle"></i>
							<h3>Je la lis,<br/> je découvre sa vie</h3>
						</div>
					</div>
					<!-- end about module -->
					<!-- about module -->
					<div class="col-md-3 text-center">
						<div class="mz-module-about">
							<i class="fa fa-bullhorn ot-circle"></i>
							<h3>J'en parle<br/> autour de moi</h3>
						</div>
					</div>
					<!-- end about module -->
					<!-- about module -->
					<div class="col-md-3 text-center">
						<div class="mz-module-about">
							<i class="fa fa-gift ot-circle"></i>
							<h3>Je gagne<br/> un des prix du défi !</h3>
						</div>
					</div>
					<!-- end about module -->
				</div>
			</div>
			<!-- /.container -->
		</section>
		<section id="inscription" >
			<div class="container">
				<div class="row">
					<div class="col-lg-12 text-center">
						<div class="section-title">
							<h2>#JeLaLis !<br/></h2>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-6">
						<div class="skills-text">
						  <h3>Le principe</h3>
							<p>
							Seule ou en équipe, je choisis et je marraine une autrice francophone oubliée pour lui donner de la visibilité.
							</p>
							<h3>Les actions du défi</h3>
							<p>
							Je suis libre de <b>m’emparer du défi comme je le souhaite</b> :
							<div class="row">
								<div class="col-md-8">
							<ul>
							<li>je trouve des infos sur elle…</li> 
							<li>je la lis…</li>
							<li>je fais un post sur les réseaux sociaux…</li>
							<li>je fais un selfie en train de la lire…</li>
							<li>je la dessine, je la joue, je l’incarne…</li>
							</ul>
								</div>
								<div class="col-md-4" style="font-size:1.5em;">
							… en utilisant le&nbsp;hashtag <b>#JeLaLis</b>
								</div>
							</div>
							</p>
							<h3>Besoin d'inspiration ?</h3>
							<p>
							&#8680; <a href="./autrices.php#actions">des <b>actions déjà réalisées</b></a><br/>
							&#8680; <a href="./pdf/activites_pedagogiques-JeLaLis.pdf">des idées <b>pour vos élèves</b></a><br/>
							&#8680; <a href="./autrices.php#faq"><b>foire aux questions</b></a><br/>
							&#8680; <a href="./images/Filigrane-Je-la-lis.png">ajouter le <b>filtre</b></a><br/>
							</p>
							<div class="skills-text">
							<h3>Les prix</h3>
							</div>
							<p>
							Tout au long du défi, <b><a href="#contact">partagez-nous vos initiatives</a></b> pour mettre en lumière l'autrice dont vous êtes marraine ou parrain,
							et ses contributions au matrimoine littéraire, afin de gagner l'un des prix du défi :
							<div class="row">
								<div class="col-md-12">
							<ul>
							<li>Prix <b>#JeLaLis-impact</b></li>
							<li>Prix <b>#JeLaLis-originalité</b></li>
							<li>Prix <b>#JeLaLis-équipe</b></li>
							</ul>
								</div>
							</div>
							Pour participer au concours et peut-être gagner l'un des prix, <b><a href="https://framaforms.org/concours-du-defi-jelalis-1567235666">inscrivez-vous ici</a></b> !<br/><i>Le <a href="https://george2etexte.files.wordpress.com/2019/09/rc3a8glement-concours-jelalis.pdf">règlement du concours</a>...</i>
							</p>
						</div>
					</div>
					<div class="col-md-6">
							<div class="skills-text">
							<h3>Inscrivez-vous au défi !</h3>
							</div>
						<form name="sentMessage" id="contactForm" action="./index.php#boutonValider" method="post">
							<a name="boutonValider"></a>
							<div class="row">
								<div class="col-md-6">
									<div class="form-group">
										<input type="text" class="form-control" placeholder="Mon prénom *" id="prenom" name="prenom" required data-validation-required-message="Merci d'entrer votre prénom.">
										<p class="help-block text-danger"></p>
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<input type="text" class="form-control" placeholder="Mon nom" id="nom" name="nom" >
										<p class="help-block text-danger"></p>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-12">
									<div class="form-group">
										<input type="email" class="form-control" placeholder="Mon courriel *" id="email" name="email" required data-validation-required-message="Merci d'entrer votre adresse de courriel.">
										<p class="help-block text-danger"></p>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-12">
									<div class="form-group">
										<input type="ville" class="form-control" placeholder="Ma ville" id="ville" name="ville">
										<p class="help-block text-danger"></p>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-12">
									<div class="form-group">
										<input type="autrice" class="form-control" placeholder="Autrice choisie (domaine public → morte depuis >70 ans) *" id="autrice" name="autrice" required data-validation-required-message="Choisissez une autrice en commençant à écrire quelques lettres de son nom…" value="<?php echo htmlspecialchars(urldecode($_GET['autrice']))?>">
										<p class="help-block text-danger"></p>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-lg-12 text-center">
									<div id="success"></div>
									<input type="hidden" id="idAutrice"name="idAutrice" value="-1"/>
									<input type="submit" class="btn" id="formOk" name="Je la lis !"/>
								</div>
							</div>
						</form>
<?php
$prenom = "";
$nom = "";
$email = "";
$ville = "";
$autrice = "";
$idAutrice = -1;
// Récupération des données de formulaire si elles existent :
if(isset($_POST["prenom"]) && isset($_POST["email"]) && isset($_POST["autrice"])){
   if(isset($_POST["prenom"])){
      $prenom = $_POST["prenom"];
   }
   if(isset($_POST["nom"])){
      $nom = $_POST["nom"];
   }
   if(isset($_POST["email"])){
      $email = $_POST["email"];
   }
   if(isset($_POST["ville"])){
      $ville = $_POST["ville"];
   }
   if(isset($_POST["autrice"])){
      $autrice = $_POST["autrice"];
   }

   $sql = "SELECT id,nomComplet FROM jelalis_autrice WHERE nomComplet=?;";
   // On envoie la requête :
   $req = $link->prepare($sql);
   $req->execute([$autrice]);
   while($data = $req->fetch()){
      $idAutrice = intval($data["id"]);
   }
   $valide = 0;
   $sql = "INSERT INTO jelalis_marraine(prenom, nom, email, ville, autrice, id_autrice, valide) VALUES (?, ?, ?, ?, ?, ?, ?);";
   // On envoie la requête :
   $req = $link->prepare($sql);
   $req->execute([$prenom, $nom, $email, $ville, $autrice, $idAutrice, $valide]);
   echo ('<div class="row"><div class="col-md-12 skills-text"><p><br/><b>Merci '.$prenom.
' ! Votre participation a été enregistrée, vous allez recevoir un courriel avec les détails pour nous tenir au courant de vos actions pour le défi #JeLaLis.</b></p></div></div>');
   $subject = 'Votre inscription au défi #JeLaLis';
   $message = "Bonjour ".$prenom." !
   
Merci pour votre inscription au défi #JeLaLis, pour promouvoir ".$autrice." pendant toute cette année 2019 !

Tenez-nous au courant de vos actions dans le cadre de ce défi en utilisant le hashtag #JeLaLis sur les réseaux sociaux, et en nous envoyant des mails à l’adresse jelalis@ledeuxiemetexte.fr à leur sujet : nous pourrons ainsi les relayer nous aussi.

Pour participer au concours permettant de gagner un des trois prix que nous remettrons en janvier 2020, #JeLaLis-impact, #JeLaLis-originalité et #JeLaLis-équipe, rendez-vous à l’adresse https://framaforms.org/concours-du-defi-jelalis-1567235666 !

Bonne participation au défi #JeLaLis et à bientôt !

-- 
L’association Le deuxième texte et ses partenaires du défi #JeLaLis : les éditions des femmes - Antoinette Fouque, le Festival International des Ecrits de Femmes, HF Île-de-France, Le Salon Des Dames, la Société Internationale pour l’Etude des Femmes de l’Ancien Régime (SIEFAR), Talents Hauts et le projet de recherche VisiAutrices.";
   $headers = "From: jelalis@ledeuxiemetexte.fr" . "\n" .
   "Reply-To: jelalis@ledeuxiemetexte.fr" . "\n" .
   "X-Mailer: PHP/" . phpversion();

   mail($email, $subject, $message, $headers);
   mail("philippe.gambette@gmail.com", $subject, $message, $headers);
}
?>

						<div class="row">
							<div class="col-md-12 skills-text">
							<p>
							<h3>Besoin d'aide pour choisir une autrice ?</h3>
							<div class="row text-center" style="margin-top:-20px;">
								<div class="col-md-3 js-choix" id="js-choix-prenom">
									<div class="mz-module-about btnchoice">
										<i class="fa fa-signature ot-circle" style="margin-bottom:-10px"></i><br/>
										Par son <br/>prénom
									</div>
								</div>
								<div class="col-md-3 js-choix" id="js-choix-anniversaire">
									<div class="mz-module-about btnchoice">
										<i class="fa fa-calendar-alt ot-circle" style="margin-bottom:-10px"></i><br/>
										Par son <br/>anniversaire
									</div>
								</div>
								<div class="col-md-3 js-choix" id="js-choix-ville">
									<div class="mz-module-about btnchoice">
										<i class="fa fa-city ot-circle" style="margin-bottom:-10px"></i><br/>
										Par sa <br/>ville
									</div>
								</div>
								<div class="col-md-3 js-choix" id="js-choix-hasard">
									<div class="mz-module-about btnchoice">
										<i class="fa fa-dice ot-circle" style="margin-bottom:-10px"></i><br/>
										Au <br/>hasard !
									</div>
								</div>
								<div class="col-md-9 js-choix-form" id="js-choix-prenom-form" style="display:none">
									<div class="mz-module-about form-autrice">
									  <div style="text-align:right;font-size:1.5em" class=""><i class="fa fa-sync-alt form-autrice-refresh-prenom"></i> <i class="fa fa-window-close form-autrice-close"></i></div>
										<form name="sentMessage" id="prenomForm" novalidate="">
											<div class="row">
												<div class="col-md-12">
													<div class="form-group">
														<input type="text" class="form-control" placeholder="Prénom de l'autrice…" id="autrice-prenom">
														<p class="help-block text-danger"></p>
													</div>
												</div>
											</div>
											<div class="row">
												<div class="col-md-12">
													<div class="form-group">
														<h3></h3>
														<img src="./images/bg-main.jpg" style="max-height:200px;max-width:400px;display:none" alt="Photo de"/>
														<br/><span id="form-autrice-source-prenom"></span>
													</div>
												</div>
											</div>
										</form>
									</div>
								</div>
								<div class="col-md-9 js-choix-form" id="js-choix-anniversaire-form" style="display:none">
									<div class="mz-module-about form-autrice">
									  <div style="text-align:right;font-size:1.5em" class=""><i class="fa fa-sync-alt form-autrice-refresh-anniv"></i> <i class="fa fa-window-close form-autrice-close"></i></div>
										<form name="sentMessage" id="anniversaireForm" novalidate="">
											<div class="row">
												<div class="col-md-6">
													<div class="form-group">
														<input type="number" class="form-control" placeholder="31" min="01" max="31" value="31" id="autrice-jour">
														<p class="help-block text-danger"></p>
													</div>
												</div>
												<div class="col-md-6">
													<div class="form-group">
														<input type="number" class="form-control" placeholder="12" min="01" max="12" value="12" id="autrice-mois">
														<p class="help-block text-danger"></p>
													</div>
												</div>
											</div>
											<div class="row">
												<div class="col-md-12">
													<div class="form-group">
														<h3></h3>
														<p id="anniversaireFormText"></p>
														<img src="./images/bg-main.jpg" style="max-height:200px;max-width:400px;display:none" alt="Photo de"/>
														<br/><span id="form-autrice-source-anniv"></span>
													</div>
												</div>
											</div>
										</form>
									</div>
								</div>
								<div class="col-md-9 js-choix-form" id="js-choix-ville-form" style="display:none">
									<div class="mz-module-about form-autrice">
									  <div style="text-align:right;font-size:1.5em" class=""><i class="fa fa-sync-alt form-autrice-refresh-ville"></i> <i class="fa fa-window-close form-autrice-close"></i></div>
										<form name="sentMessage" id="villeForm" novalidate="">
											<div class="row">
												<div class="col-md-12">
													<div class="form-group">
														<input type="text" class="form-control" placeholder="Ville de l'autrice…" id="autrice-ville">
														<p class="help-block text-danger"></p>
													</div>
												</div>
											</div>
											<div class="row">
												<div class="col-md-12">
													<div class="form-group">
														<h3></h3>
														<p id="villeFormText"></p>
														<img src="./images/bg-main.jpg" style="max-height:200px;max-width:400px;display:none" alt="Photo de"/>
														<br/><span id="form-autrice-source-ville"></span>
													</div>
												</div>
											</div>
										</form>
									</div>
								</div>
								<div class="col-md-9 js-choix-form" id="js-choix-hasard-form" style="display:none">
									<div class="mz-module-about form-autrice">									
									  <div style="text-align:right;font-size:1.5em" class=""><i class="fa fa-sync-alt form-autrice-refresh-alea"></i> <i class="fa fa-window-close form-autrice-close"></i></div>
										<form name="sentMessage" id="aleatoireForm" novalidate="">
											<div class="row">
												<div class="col-md-12">
													<div class="form-group">
														<h3></h3>
														<img src="" style="max-height:200px;max-width:400px" alt="Photo de"/>
														<br/><span id="form-autrice-source-alea"></span>
													</div>
												</div>
											</div>
										</form>
									</div>
								</div>
							</div>
						  <!--
							Vous ne voyez pas quelle femme de lettres choisir ? 
							Allez voir dans le <i><a href="https://www.dictionnaire-creatrices.com/domaine-litteratures-et-livres/secteur-litterature">Dictionnaire universel des créatrices</a></i> des éditions des femmes - Antoinette Fouque,
							dans le <a href="http://ledeuxiemetexte.fr">moteur de recherche des autrices contemporaines d'un auteur</a> du <i>Deuxième texte</i>,
							ou dans <a href="https://fr.wikipedia.org/wiki/Cat%C3%A9gorie:Femme_de_lettres">Wikipédia</a>.
							-->
							</p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
		<section class="overlay-dark bg-img1 dark-bg short-section" id="autrices">
			<div class="container text-center">
				<div class="row">
					<div class="col-md-6 mb-sm-30">
						<div class="counter-item">
<?php
   $sql = "SELECT id_marraine,valide FROM jelalis_marraine WHERE valide=1;";
   // On envoie la requête :
   $req = $link->prepare($sql);
   $req->execute();
   $nbChoix = $req->rowCount();

   $sql = "SELECT valide, DISTINCT id_autrice FROM jelalis_marraine WHERE valide=1;";
   // On envoie la requête :
   $req = $link->prepare($sql);
   $req->execute();
   $nbChoisies = $req->rowCount();
   
   $sql = "SELECT DISTINCT id FROM jelalis_autrice WHERE 1;";
   // On envoie la requête :
   $req = $link->prepare($sql);
   $req->execute();
   $nbAutrices = $req->rowCount() - $nbChoisies;

?>
   							<h2 data-count="<?php echo $nbChoix; ?>"><?php echo $nbChoix; ?></h2>
							<h6>choix d'autrices</h6>
						</div>
					</div>
					<div class="col-md-6 mb-sm-30">
						<div class="counter-item">
							<h2 data-count="<?php echo $nbAutrices; ?>"><?php echo $nbAutrices; ?></h2>
							<h6>autrices à choisir</h6>
						</div>
					</div>
				</div>
			</div>
		</section>
		<section>
			<div class="container">
				<div class="row">
					<div class="col-lg-12 text-center">
						<div class="section-title">
							<h2>AUTRICES</h2>
							<p>Les vingt dernières autrices choisies :</p>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-lg-12 text-center">
						<div class="owl-carousel">
<?php

   $sql = "SELECT autrice,prenom FROM jelalis_marraine WHERE valide=1 ORDER BY id_marraine DESC LIMIT 20;";
   // On envoie la requête :
   $req = $link->prepare($sql);
   $req->execute();
   while($data = $req->fetch()){
      echo '
							<div class="item">
								<div class="partner-logo"><b>'.htmlspecialchars($data["autrice"]).'</b><br/>par<br/>'.htmlspecialchars($data["prenom"]).'</div>
							</div>
      ';
   }

?>
<!--
							<div class="item">
								<div class="partner-logo"><b>Marie Leprince de Beaumont</b><br/>par<br/>Jeanne</div>
							</div>
							<div class="item">
								<div class="partner-logo"><b>Marie Lenéru</b><br/>par<br/>Philippe</div>
							</div>
							<div class="item">
								<div class="partner-logo"><b>Marie d'Agoult</b><br/>par<br/>Constance</div>
							</div>
							<div class="item">
								<div class="partner-logo"><b>Anna de Noailles</b><br/>par<br/>Chloé</div>
							</div>
							<div class="item">
								<div class="partner-logo"><b>Marie-Anne Robert</b><br/>par<br/>Françoise</div>
							</div>
							<div class="item">
								<div class="partner-logo"><b>Catherine Des Roches</b><br/>par<br/>Laura</div>
							</div>
							<div class="item">
								<div class="partner-logo"><b>Jeanne-Yves Blanc</b><br/>par<br/>Anne</div>
							</div>
							<div class="item">
								<div class="partner-logo"><b>Anne Wiazemsky</b><br/>par<br/>Thibaut</div>
							</div>
							<div class="item">
								<div class="partner-logo"><b>Kim Thúy</b><br/>par<br/>Cécile</div>
							</div>
							<div class="item">
								<div class="partner-logo"><b>Mademoiselle Mars (Anne Boutet)</b><br/>par<br/>Gwladys</div>
							</div>
							<div class="item">
								<div class="partner-logo"><b>Marguerite de Valois</b><br/>par<br/>Eliane</div>
							</div>
							<div class="item">
								<div class="partner-logo"><b>Charlotte Duplessis-Mornay</b><br/>par<br/>Mallory</div>
							</div>
							<div class="item">
								<div class="partner-logo"><b>Marie-Catherine de Villedieu</b><br/>par<br/>Edwige</div>
							</div>
							<div class="item">
								<div class="partner-logo"><b>Marceline Desbordes-Valmore</b><br/>par<br/>Christine</div>
							</div>
							<div class="item">
								<div class="partner-logo"><b>Madeleine Des Roches</b><br/>par<br/>Caroline</div>
							</div>
							<div class="item">
								<div class="partner-logo"><b>Marguerite de Navarre</b><br/>par<br/>Scott</div>
							</div>
-->
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-lg-12 text-center">
					   <br/><br/>Découvrez :<br/>
					   &#8680; <a href="./autrices.php#autrices">toutes les <b>autrices choisies</b> pour le défi #JeLaLis</a><br/>
					   &#8680; <a href="./autrices.php#carte">la carte des <b>autrices pas encore choisies</b> pour le défi #JeLaLis</a><br/>
					   &#8680; <a href="./autrices.php#actions">les <b>actions réalisées</b> pour le défi #JeLaLis</a>
					</div>
				</div>
			</div>
		</section>
		<section id="team" class="light-bg">
			<div class="container">
				<div class="row">
					<div class="col-lg-12 text-center">
						<div class="section-title">
							<h2>PARTENAIRES</h2>
							<p></p>
						</div>
					</div>
				</div>
				<div class="row">
					<!-- team member item -->
					<div class="col-md-3">
						<div class="team-item">
							<div class="team-image">
								<img src="images/Logo-Le_deuxieme_texte.png" class="img-responsive" alt="author">
							</div>
							<div class="team-text">
								<h3><a href="http://ledeuxiemetexte.fr">Le deuxième texte</a></h3>
								<div class="team-location"></div>
								<div class="team-position"></div>
								<p>L’association <i>Le deuxième texte</i> a pour objet la mise en valeur des femmes de lettres dans le «patrimoine» culturel francophone. Elle développe une plateforme web pour permettre aux profs de lettres de partager des extraits des textes écrits par des femmes.</p>
							</div>
						</div>
					</div>
					<!-- end team member item -->
					<!-- team member item -->
					<div class="col-md-3">
						<div class="team-item">
							<div class="team-image">
								<img src="./images/Logo-Editions_des_femmes_Antoinette_Fouque.png" class="img-responsive" alt="author">
							</div>
							<div class="team-text">
								<h3><a href="https://www.desfemmes.fr/">Éditions des femmes - <br/>Antoinette Fouque</a></h3>
								<div class="team-location"></div>
								<div class="team-position"></div>
								<p>Le désir qui a motivé la naissance des <i>éditions des femmes - Antoinette Fouque</i> est davantage politique qu’éditorial : à travers la maison d’édition, c’est la libération des femmes qu’il s’agit de faire avancer.</p>
							</div>
						</div>
					</div>
					<!-- end team member item -->
					<!-- team member item -->
					<div class="col-md-3">
						<div class="team-item">
							<div class="team-image">
								<img src="images/Logo-Le_salon_des_dames.png" class="img-responsive" alt="author">
							</div>
							<div class="team-text">
								<h3><a href="https://www.lesalondesdames.paris/">Le Salon des Dames</a></h3>
								<div class="team-location"></div>
								<div class="team-position"></div>
								<p>LSD : Liberté, Sororité, Diversité. Si tout le monde prenait du féminisme, on serait tous défoncé·es à la liberté. Le <i>Salon des Dames</i> est une ONG qui repense la place des femmes dans la société, booste les petites filles à avoir de grandes ailes et empowerise les femmes au quotidien.</p>
							</div>
						</div>
					</div>
					<!-- end team member item -->
					<!-- team member item -->
					<div class="col-md-3">
						<div class="team-item">
							<div class="team-image">
								<img src="images/Logo-Festival_international_des_ecrits_de_femmes.png" class="img-responsive" alt="author">
							</div>
							<div class="team-text">
								<h3><a href="http://ecritsdefemmes.fr">Festival International des Ecrits de Femmes</a></h3>
								<div class="team-location"></div>
								<div class="team-position"></div>
								<p>Le <i>FIEF</i>, <i>Festival international des écrits de femmes</i> réunit le deuxième week-end d’octobre, à la Maison de Colette, des lecteurs, des écrivains, des artistes et des chercheurs venus du monde entier se retrouver autour d’un thème à chaque fois différent.</p>
							</div>
						</div>
					</div>
					<!-- end team member item -->
				</div>
				<div class="row">
					<!-- team member item -->
					<div class="col-md-3">
						<div class="team-item">
							<div class="team-image">
								<img src="images/Logo-Talents_hauts.png" class="img-responsive" alt="author">
							</div>
							<div class="team-text">
								<h3><a href="http://www.talentshauts.fr/">Talents hauts</a></h3>
								<div class="team-location"></div>
								<div class="team-position"></div>
								<p><i>Talents hauts</i> est une maison d’édition jeunesse indépendante créée en 2005, qui publie des livres pour la jeunesse, des livres percutants, forts, drôles, qui bousculent les idées reçues. Sa collection <i>Plumées</i> vise à retrouver, rééditer, réhabiliter les œuvres du matrimoine.</p>
							</div>
						</div>
					</div>
					<!-- end team member item -->
					<!-- team member item -->
					<div class="col-md-3">
						<div class="team-item">
							<div class="team-image">
								<img src="images/Logo-VisiAutrices.png" class="img-responsive" alt="author">
							</div>
							<div class="team-text">
								<h3><a href="http://visiautrices.hypotheses.org">VisiAutrices</a></h3>
								<div class="team-location"></div>
								<div class="team-position"></div>
								<p>Le projet de recherche <i>VisiAutrices</i>, financé par le CNRS et le RnMSH, vise à mesurer et caractériser la place des femmes dans l’enseignement des lettres dans le secondaire et le supérieur, et son évolution.</p>
							</div>
						</div>
					</div>
					<!-- end team member item -->
					<!-- team member item -->
					<div class="col-md-3">
						<div class="team-item">
							<div class="team-image">
								<img src="./images/Logo-SIEFAR.png" class="img-responsive" alt="author">
							</div>
							<div class="team-text">
								<h3><a href="http://siefar.org">SIEFAR</a></h3>
								<div class="team-location"></div>
								<div class="team-position"></div>
								<p>La <i>Société Internationale pour l’Étude des Femmes de l’Ancien Régime</i>, société savante née en 2000, a pour vocation l’étude des conditions de vie, des actions, des &oelig;uvres et de la pensée des femmes des périodes précédant la Révolution française.</p>
							</div>
						</div>
					</div>
					<!-- end team member item -->
					<!-- team member item -->
					<div class="col-md-3">
						<div class="team-item">
							<div class="team-image">
								<img src="images/Logo-HF_ile_de_france.png" class="img-responsive" alt="author">
							</div>
							<div class="team-text">
								<h3><a href="http://hf-idf.org/">HF Île-de-France</a></h3>
								<div class="team-location"></div>
								<div class="team-position"></div>
								<p><i>HF Île-de-France</i> a pour objet le repérage des inégalités entre les femmes et les hommes dans les milieux de l’art et de la culture, la mobilisation contre les discriminations observées, et l’évolution vers l’égalité réelle et la parité.</p>
							</div>
						</div>
					</div>
					<!-- end team member item -->
				</div>
			</div>
		
		</section>
		
		<section id="contact">
			<div class="container">
				<div class="row">
					<div class="col-lg-12 text-center">
						<div class="section-title">
							<h2>CONTACT</h2>
							<p>Avez-vous jeté un oeil à notre <a href="http://ledeuxiemetexte.fr/jelalis/autrices.php#faq">foire aux questions</a> ?
							<br/>
							Contactez-nous à l'adresse <a href="mailto:jelalis@ledeuxiemetexte.fr">jelalis@ledeuxiemetexte.fr</a> pour toute question sur cette opération.</p>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-lg-12 text-center">
						<div class="section-title">
							<h3>Sources des illustrations</h3>
							<p>
							Tableau <i>Les éclaireuses</i> d'Amélie Beaury-Saurel, exposé au Salon de Paris en 1914<br/>
							(source&nbsp;: <a href="https://bibliotheques-specialisees.paris.fr/ark:/73873/pf0000858991">Ville de Paris / Bibliothèque Marguerite&nbsp;Durand</a>).
							<br/><br/>
							Informations relatives aux autrices et portraits extraits de leur fiche<br/>sur <a href="http://data.bnf.fr">data.bnf</a> (Bibliothèque nationale de France, <a href="https://data.bnf.fr/sparql?default-graph-uri=&query=PREFIX+foaf%3A+%3Chttp%3A%2F%2Fxmlns.com%2Ffoaf%2F0.1%2F%3E%0D%0APREFIX+rdarelationships%3A+%3Chttp%3A%2F%2Frdvocab.info%2FRDARelationshipsWEMI%2F%3E%0D%0APREFIX+dcterms%3A+%3Chttp%3A%2F%2Fpurl.org%2Fdc%2Fterms%2F%3E%0D%0APREFIX+bio%3A+%3Chttp%3A%2F%2Fvocab.org%2Fbio%2F0.1%2F%3E%0D%0APREFIX+bnf-onto%3A+%3Chttp%3A%2F%2Fdata.bnf.fr%2Fontology%2Fbnf-onto%2F%3E%0D%0APREFIX+skos%3A+%3Chttp%3A%2F%2Fwww.w3.org%2F2004%2F02%2Fskos%2Fcore%23%3E%0D%0APREFIX+rdagroup2elements%3A+%3Chttp%3A%2F%2Frdvocab.info%2FElementsGr2%2F%3E%0D%0ASELECT+DISTINCT+%3Fautrice+%3FnomComplet+%3FlieuNaissance+%3FlieuMort+%28MIN%28%3Fim%29+AS+%3Fimage%29+%3Fprenom+%3Fisni+%3Fnom+%3Fnaissance+%3Fmort++WHERE+%7B%0D%0A++%3Fexpr+dcterms%3Acontributor+%3Fautrice+.%0D%0A++%3Fexpr+%3Chttp%3A%2F%2Fdata.bnf.fr%2Fvocabulary%2Froles%2Fr70%3E+%3Fautrice.%0D%0A++%3Fman+rdarelationships%3AexpressionManifested+%3Fexpr.%0D%0A++%3Fman+dcterms%3Adate+%3Fdate.%0D%0A++FILTER%28%3Fdate%3C%221949%22%29.%0D%0A++%3Fexpr+dcterms%3Atype+%3Chttp%3A%2F%2Fpurl.org%2Fdc%2Fdcmitype%2FText%3E.%0D%0A++%3Fexpr+dcterms%3Alanguage+%3Chttp%3A%2F%2Fid.loc.gov%2Fvocabulary%2Fiso639-2%2Ffre%3E.%0D%0A++%3Fautrice+bio%3Adeath+%3Fmort.%0D%0A++OPTIONAL%7B%3Fautrice+bio%3Abirth+%3Fnaissance.%7D%0D%0A++%3Fautrice+foaf%3Agender+%22female%22.%0D%0A++OPTIONAL%7B%3Fautrice+foaf%3Adepiction+%3Fim.%7D%0D%0A++OPTIONAL%7B%3Fautrice+rdagroup2elements%3AplaceOfBirth+%3FlieuNaissance.%7D+%0D%0A++OPTIONAL%7B%3Fautrice+rdagroup2elements%3AplaceOfDeath+%3FlieuMort%7D.%0D%0A++FILTER%28STRLEN%28%3Fmort%29+%3E%3D+4+%26%26+SUBSTR%28%3Fmort%2C1%2C4%29%3C%221949%22+%26%26+%3Fmort+%21%3D+%2219..%22%29%0D%0A++OPTIONAL+%7B%0D%0A++++%3FcIsni+foaf%3Afocus+%3Fautrice+.%0D%0A++++%3FcIsni+isni%3AidentifierValid+%3Fisni+.%0D%0A++%7D%0D%0A++OPTIONAL+%7B%3Fautrice+foaf%3Aname+%3FnomComplet+.%7D%0D%0A++OPTIONAL+%7B%3Fautrice+foaf%3AfamilyName+%3Fnom+.%7D%0D%0A++OPTIONAL+%7B%3Fautrice+foaf%3AgivenName+%3Fprenom+.%7D%0D%0A++OPTIONAL+%7B%3Fautrice+foaf%3AlanguageOfThePerson+%3Flang+.%7D%0D%0A%7D+GROUP+BY+%3Fautrice+%3FnomComplet+%3Fisni+%3Fprenom+%3Fnom+%3Fmort+%3Fnaissance+%3FlieuNaissance+%3FlieuMort%0D%0AORDER+BY+DESC%28%3Fnaissance%29&format=text%2Fhtml&timeout=0&should-sponge=&debug=on">requête utilisée</a>) ou sur <a href="http://wikidata.org">Wikidata</a> (<a href="https://query.wikidata.org/embed.html#%23Autrices%20de%20langue%20fran%C3%A7aises%20mortes%20avant%201949%0ASELECT%20%3Fautrice%20%3FautriceLabel%20%3FlieuNaissance%20%3FlieuNaissanceLabel%20%3FlieuMort%20%3FlieuMortLabel%20%28MIN%28%3Fimg%29%20AS%20%3Fimage%29%20%3FprenomLabel%20%28MIN%28%3FidIsni%29%20as%20%3Fisni%29%20%3FidBnf%20%28MIN%28%3Fn%29%20AS%20%3Fnaissance%29%20%28MAX%28%3Fm%29%20AS%20%3Fmort%29%20WHERE%20%7B%0A%20%20%3Fautrice%20wdt%3AP106%20wd%3AQ36180.%0A%20%20%3Fautrice%20wdt%3AP21%20wd%3AQ6581072.%0A%20%20%3Fautrice%20wdt%3AP570%20%3Fm.%0A%20%20%7B%20%3Fautrice%20wdt%3AP1412%20wd%3AQ150.%20%7D%0A%20%20UNION%0A%20%20%7B%20%3Fautrice%20wdt%3AP1412%20wd%3AQ1473289.%20%7D%0A%20%20OPTIONAL%20%7B%20%3Fautrice%20wdt%3AP569%20%3Fn.%7D%0A%20%20OPTIONAL%20%7B%20%3Fautrice%20wdt%3AP19%20%3FlieuNaissance.%7D%0A%20%20OPTIONAL%20%7B%20%3Fautrice%20wdt%3AP20%20%3FlieuMort.%7D%0A%20%20OPTIONAL%20%7B%20%3Fautrice%20wdt%3AP18%20%3Fimg.%20%7D%0A%20%20OPTIONAL%20%7B%20%3Fautrice%20wdt%3AP735%20%3Fprenom.%20%7D%0A%20%20OPTIONAL%20%7B%20%3Fautrice%20wdt%3AP213%20%3FidIsni.%20%7D%0A%20%20OPTIONAL%20%7B%20%3Fautrice%20wdt%3AP268%20%3FidBnf.%20%7D%0A%20%20SERVICE%20wikibase%3Alabel%20%7B%20bd%3AserviceParam%20wikibase%3Alanguage%20%22%5BAUTO_LANGUAGE%5D%2Cfr%2Cen%22.%20%7D%0A%20%20FILTER%28%28YEAR%28%3Fm%29%29%20%3C%201949%29%0A%7D%0AGROUP%20BY%20%3Fautrice%20%3FautriceLabel%20%3FlieuNaissance%20%3FlieuNaissanceLabel%20%3FlieuMort%20%3FlieuMortLabel%20%3Fimage%20%3FprenomLabel%20%3Fisni%20%3FidBnf%20%3Fmort%20%3Fnaissance%0AORDER%20BY%20DESC%28%3Fnaissance%29">requête utilisée</a>).
							<br/><br/>
							Icônes tirées de <i><a href="http://fontawesome.com">FontAwesome</a></i>
							</p>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-lg-12 text-center">
						<div class="section-title">
							<h3>À propos de vos données personnelles…</h3>
							<p>
							Les informations personnelles que vous déposerez dans le formulaire ci-dessus seront traitées par l'association <i>ledeuxiemetexte.fr</i>, 
							auprès de laquelle vous pouvez exercer vos droits d'accès, rectification et suppression à l'adresse <i>rgpd@ledeuxiemetexte.fr</i>.
							L'adresse mail sera utilisée pour vous contacter seulement par l'association, à propos de cette opération <i>#JeLaLis&nbsp;!</i> et
							de moyens de faire connaître des femmes de lettres.
							<!--Si vous le souhaitez, en cochant la case adéquate, elle pourra apparaître
							sur notre site pour que les personnes intéressées par la femme de lettres que vous avez choisie puissent vous contacter.-->
							Indiquer votre ville nous permettra de vous signaler des événements sur les femmes de lettres qui pourraient vous intéresser,
							à proximité de votre adresse. 
							</p>
						</div>
					</div>
				</div>
			</div>
		</section>
		<p id="back-top">
			<a href="#top"><i class="fa fa-angle-up"></i></a>
		</p>
		<footer>
			<div class="container text-center">
				<p>Thème conçu par <a href="http://moozthemes.com"><span>MOOZ</span>Themes.com</a> - Code source de ce site disponible <a href="https://framagit.org/george2etexte/jelalis/">sur Framagit</a>.</p>
			</div>
		</footer>


		<!-- Bootstrap core JavaScript
			================================================== -->
		<!-- Placed at the end of the document so the pages load faster -->
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
		<script src="http://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
		<script src="js/bootstrap.min.js"></script>
		<script src="js/owl.carousel.min.js"></script>
		<script src="js/cbpAnimatedHeader.js"></script>
		<script src="js/theme-scripts.js"></script>
		<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
		<script src="js/ie10-viewport-bug-workaround.js"></script>
		<script>
			$(document).ready(function(){

         var nomsMois = ["","janvier","février","mars","avril","mai","juin","juillet","août","septembre","octobre","novembre","décembre"];
         
         
         $(".js-donnees-infos").hide();
			   var boutonsAffiches = 1;
			   
			   $(".form-autrice-close").on("click",function(){
               $(".js-choix").show();
			         $(".js-choix-form").hide();
               boutonsAffiches = 1;
         })
         
			   $(".form-autrice-refresh-alea").on("click",function(){
               chargeAlea();
         })
			   $(".form-autrice-refresh-anniv").on("click",function(){
               chargeAnniv();
         })
			   $(".form-autrice-refresh-ville").on("click",function(){
               chargeVille();
         })
			   $(".form-autrice-refresh-prenom").on("click",function(){
               chargePrenom();
         })

         $("#autrice").on("keyup",function(){
            $("#idAutrice").val("");
         })
			   
			   // Aide au choix de l'autrice
			   // critere vaut "ville" ou "anniversaire" ou "prenom"
			   function chargeAutrice(critere){
			      if($("#autrice-"+critere).val().length>1){
			         $.get("./api.php?action=3&ville="+$("#autrice-ville").val(),function(data){
			            if(data.image.length>0){
				            $("#"+critere+"Form img").attr("src",data.image.replace(".PNG",".jpg").replace(".gif",".jpg").replace(".png",".jpg").replace("http://commons.wikimedia.org/wiki/Special:FilePath/","http://ledeuxiemetexte.fr/jelalis/autrices/"));
				            $("#"+critere+"Form img").attr("alt","Portrait de "+data.nomComplet);
				            $("#"+critere+"Form img").show();
				          } else {
				            $("#"+critere+"Form img").hide();
				          }
			            if(data.idBnf != ""){
			               $("#"+critere+"Form h3").html('<a href="https://data.bnf.fr/fr/'+data.idBnf.substring(0,data.idBnf.length-1)+'">'+data.nomComplet+'</a>');
			            } else {
			               $("#"+critere+"Form h3").html('<a href="https://www.wikidata.org/wiki/'+data.idWikidata+'">'+data.nomComplet+'</a>');			               
			            }
			            $("#autrice").val(data.nomComplet);
			            $("#idAutrice").val(data.id);
			         })
			      }
			   }
			   
			   // Choix de la ville
			   function chargeVille(){
			      if($("#autrice-ville").val().length>1){
			         $.get("./api.php?action=3&ville="+$("#autrice-ville").val(),function(data){
			            $("#form-autrice-source-ville").html('');
			            if(data.image.length>0){
				            $("#villeForm img").attr("src",data.image.replace(".PNG",".jpg").replace(".gif",".jpg").replace(".png",".jpg").replace("http://commons.wikimedia.org/wiki/Special:FilePath/","http://ledeuxiemetexte.fr/jelalis/autrices/"));
				            $("#form-autrice-source-ville").html('<small><a href="'+data.image+'">source</a></small>');
				            $("#villeForm img").attr("alt","Portrait de "+data.nomComplet);
				            $("#villeForm img").show();
				          } else {
				            $("#villeForm img").hide();
				          }
			            if(data.idBnf != ""){
			               $("#villeForm h3").html('<a href="https://data.bnf.fr/fr/'+data.idBnf.substring(0,data.idBnf.length-1)+'">'+data.nomComplet+'</a>');
			            } else {
			               $("#villeForm h3").html('<a href="https://www.wikidata.org/wiki/'+data.idWikidata+'">'+data.nomComplet+'</a>');			               
			            }
			            if((data.lieuMort.length>0) && (($("#autrice-ville").val() == data.lieuMort.substring(0,$("#autrice-ville").val().length)) || ($("#autrice-ville").val().substring(0,data.lieuMort.length) == data.lieuMort))){
			                $("#villeFormText").html("(morte à "+data.lieuMort+")");
			            }
			            if((data.lieuNaissance.length>0) && (($("#autrice-ville").val() == data.lieuNaissance.substring(0,$("#autrice-ville").val().length)) || ($("#autrice-ville").val().substring(0,data.lieuNaissance.length) == data.lieuNaissance))){
			                $("#villeFormText").html("(née à "+data.lieuNaissance+")");
			            }
			            $("#autrice").val(data.nomComplet);
			            $("#idAutrice").val(data.id);
			         })
			      }
			   }
			   $("#autrice-ville").on("keyup",chargeVille)
			   
			   // Choix du prénom
			   function chargePrenom(){
			      if($("#autrice-prenom").val().length>1){
			         $.get("./api.php?action=1&prenom="+$("#autrice-prenom").val(),function(data){
			            $("#form-autrice-source-prenom").html('');
			            if(data.image.length>0){
				            $("#prenomForm img").attr("src",data.image.replace(".PNG",".jpg").replace(".gif",".jpg").replace(".png",".jpg").replace("http://commons.wikimedia.org/wiki/Special:FilePath/","http://ledeuxiemetexte.fr/jelalis/autrices/"));
				            $("#form-autrice-source-prenom").html('<small><a href="'+data.image+'">source</a></small>')
				            $("#prenomForm img").attr("alt","Portrait de "+data.nomComplet);
				            $("#prenomForm img").show();
				          } else {
				            $("#prenomForm img").hide();
				          }
			            if(data.idBnf != ""){
			               $("#prenomForm h3").html('<a href="https://data.bnf.fr/fr/'+data.idBnf.substring(0,data.idBnf.length-1)+'">'+data.nomComplet+'</a>');
			            } else {
			               $("#prenomForm h3").html('<a href="https://www.wikidata.org/wiki/'+data.idWikidata+'">'+data.nomComplet+'</a>');			               
			            }
			            $("#autrice").val(data.nomComplet);
			            $("#idAutrice").val(data.id);
			         })
			      }
			   }
			   $("#autrice-prenom").on("keyup",chargePrenom)
			   
			   // Choix de l'anniversaire
			   function chargeAnniv(){
			         var mois = $("#autrice-mois").val();
			         var jour = $("#autrice-jour").val();
			         if(mois.length<2){
			            mois = "0"+mois;
			         }
			         if(jour.length<2){
			            jour = "0"+jour;
			         }
			         $.get("./api.php?action=2&anniv="+mois+"-"+jour,function(data){
			            $("#form-autrice-source-anniv").html('');
			            if(data.image.length>0){
				            $("#anniversaireForm img").attr("src",data.image.replace(".PNG",".jpg").replace(".gif",".jpg").replace(".png",".jpg").replace("http://commons.wikimedia.org/wiki/Special:FilePath/","http://ledeuxiemetexte.fr/jelalis/autrices/"));
				            $("#form-autrice-source-anniv").html('<small><a href="'+data.image+'">source</a></small>')
				            $("#anniversaireForm img").attr("alt","Portrait de "+data.nomComplet);
				            $("#anniversaireForm img").show()
				          } else {
				            $("#anniversaireForm img").hide()
				          }
			            if(data.idBnf != ""){
			               $("#anniversaireForm h3").html('<a href="https://data.bnf.fr/fr/'+data.idBnf.substring(0,data.idBnf.length-1)+'">'+data.nomComplet+'</a>');
			            } else {
			               $("#anniversaireForm h3").html('<a href="https://www.wikidata.org/wiki/'+data.idWikidata+'">'+data.nomComplet+'</a>');			               
			            }
			            jour = parseInt(jour);
			            if(jour==1){
			               jour = "1<sup>er</sup>";
			            }
			            $("#anniversaireFormText").html("("+jour+" "+nomsMois[parseInt(mois)]+")")
			            $("#autrice").val(data.nomComplet);
			            $("#idAutrice").val(data.id);
			         })
			   }
			   
			   
			   // Choix aléatoire
			   function chargeAlea(){
			               $.get("./api.php?action=0",function(data){
			               $("#aleatoireForm img").attr("src",data.image.replace(".PNG",".jpg").replace(".gif",".jpg").replace(".png",".jpg").replace("http://commons.wikimedia.org/wiki/Special:FilePath/","http://ledeuxiemetexte.fr/jelalis/autrices/"));
				             $("#form-autrice-source-alea").html('<small><a href="'+data.image+'">source</a></small>')
			               $("#aleatoireForm img").attr("alt","Portrait de "+data.nomComplet);
			               if(data.idBnf != ""){
			                  $("#aleatoireForm h3").html('<a href="https://data.bnf.fr/fr/'+data.idBnf.substring(0,data.idBnf.length-1)+'" target="_blank">'+data.nomComplet+'</a>');
			               } else {
			                  $("#aleatoireForm h3").html('<a href="https://www.wikidata.org/wiki/'+data.idWikidata+'" target="_blank">'+data.nomComplet+'</a>');			               
			               }
			               $("#autrice").val(data.nomComplet);
			               $("#idAutrice").val(data.id);
			            })
         }

			   $("#autrice-mois").on("keyup",chargeAnniv)
			   $("#autrice-jour").on("keyup",chargeAnniv)
			   $("#autrice-mois").on("change",chargeAnniv)
			   $("#autrice-jour").on("change",chargeAnniv)

			   
			   $(".js-choix").on("click",function(){
			      // Clic sur un des boutons d'aide au choix d'autrice
			      if(boutonsAffiches == 1){
			         $(".js-choix").hide();
			         $(this).show();
			         $("#"+$(this).attr("id")+"-form").show();
			         boutonsAffiches = 0;
			         if($("#prenom").val().length > 0){
			            $("#autrice-prenom").val($("#prenom").val());
			         }
			         if($("#ville").val().length > 0){
			            $("#autrice-ville").val($("#ville").val());
			         }
			         // Choix du prénom de l'autrice :
			         if($(this).attr("id")=="js-choix-prenom"){
			            chargePrenom();
			         }
			         // Choix de l'anniversaire de l'autrice :
			         if($(this).attr("id")=="js-choix-anniversaire"){
			            chargeAnniv();
			         }
			         // Choix de la ville de l'autrice :
			         if($(this).attr("id")=="js-choix-ville"){
			            chargeVille();
			         }
			         // Choix aléatoire de l'autrice :
			         if($(this).attr("id")=="js-choix-hasard"){
			            chargeAlea();
			         }
            } else {
               $(".js-choix").show();
			         $(".js-choix-form").hide();
               boutonsAffiches = 1;
            }			        
			   })

			   $(".js-donnees").on("click",function(){
			      $(".js-donnees-infos").toggle();
         })
         
			   $(".js-donnees-infos").on("click",function(){
			      $(".js-donnees-infos").hide();
         })
                  
         // autocomplete autrice
         var liste = [
<?php
   $sql = "SELECT id, nomComplet FROM jelalis_autrice WHERE 1;";
   // On envoie la requête :
   $req = $link->prepare($sql);
   $req->execute();
   $i = 0;
   while($data = $req->fetch()){
      if($i>0){
      echo ",";
      }
      echo '"'.$data["nomComplet"].'"';
      $i+=1;
   }
?>
         ];

         $('#autrice').autocomplete({
             source : liste
         });
			})
		
		</script>
	</body>
</html>