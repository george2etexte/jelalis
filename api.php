<?php

include("connect.php");

if(isset($_GET['action'])){
   header("Access-Control-Allow-Origin: *");
   header('Content-Type: application/json');
   $action = intval($_GET['action']);
   if($action==0){
      // Choix aléatoire
      // On crée la requête SQL :
      $sql = "SELECT * FROM jelalis_autrice WHERE image<>'' ORDER BY RAND() LIMIT 1";
      // On envoie la requête :
      $req = $link->prepare($sql);
      $req->execute();
      while($data = $req->fetch()){
         $result = json_encode($data);
      }
      echo $result;
   }
   if($action==1){
      // Choix par prénom
      // On crée la requête SQL :
      $sql = "SELECT * FROM jelalis_autrice WHERE prenom LIKE ? ORDER BY RAND() LIMIT 1";
      // On envoie la requête :
      $req = $link->prepare($sql);
      $req->execute(array($_GET['prenom'].'%'));
      while($data = $req->fetch()){
         $result = json_encode($data);
      }
      echo $result;
   }
   if($action==2){
      // Choix par anniversaire
      // On crée la requête SQL :
      $sql = "SELECT * FROM jelalis_autrice WHERE anniversaire=? ORDER BY RAND() LIMIT 1";
      // On envoie la requête :
      $req = $link->prepare($sql);
      $req->execute(array($_GET['anniv']));
      while($data = $req->fetch()){
         $result = json_encode($data);
      }
      echo $result;
   }
   if($action==3){
      // Choix par ville
      // On crée la requête SQL :
      $sql = "SELECT * FROM jelalis_autrice WHERE (lieuNaissance LIKE ?) OR (lieuMort LIKE ?) ORDER BY RAND() LIMIT 1";
      // On envoie la requête :
      $req = $link->prepare($sql);
      $req->execute(array($_GET['ville'].'%',$_GET['ville'].'%'));
      while($data = $req->fetch()){
         $result = json_encode($data);
      }
      echo $result;
   }
} else {
// On fait une boucle qui va faire un tour pour chaque enregistrement :
echo '
<html>
<head>
<meta charset="utf-8">
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous" />
<link rel="stylesheet" href="style.css" />

</head>
<body>
<div class="container">
<h1>API Femmes de lettres dans le domaine public</h1>

<h2>Documentation en français</h2>
<p><ul>Paramètre <tt>action</tt> :
<li>0 : choix aléatoire</li>
</ul>
</p>

</div>
</body>
</html>';
}



?>